package application;

import java.util.ArrayList;
import java.util.List;

public class CarteRestaurant {
    private List<Plat> entrees;
    private List<Plat> platsPrincipaux;
    private List<Plat> desserts;
    private List<Boisson> boissons;

    public CarteRestaurant() {
        this.entrees = new ArrayList<>();
        this.platsPrincipaux = new ArrayList<>();
        this.desserts = new ArrayList<>();
        this.boissons = new ArrayList<>();
    }

    public void ajouterEntree(Plat plat) {
        entrees.add(plat);
    }

    public void supprimerEntree(Plat plat) {
        entrees.remove(plat);
    }

    public void ajouterPlatPrincipal(Plat plat) {
        platsPrincipaux.add(plat);
    }

    public void supprimerPlatPrincipal(Plat plat) {
        platsPrincipaux.remove(plat);
    }

    public void ajouterDessert(Plat plat) {
        desserts.add(plat);
    }

    public void supprimerDessert(Plat plat) {
        desserts.remove(plat);
    }

    public void ajouterBoisson(Boisson boisson) {
        boissons.add(boisson);
    }

    public void supprimerBoisson(Boisson boisson) {
        boissons.remove(boisson);
    }

    public List<Plat> getEntrees() {
        return entrees;
    }

    public List<Plat> getPlatsPrincipaux() {
        return platsPrincipaux;
    }

    public List<Plat> getDesserts() {
        return desserts;
    }

    public List<Boisson> getBoissons() {
        return boissons;
    }
    public void initCarte() {
        // Initialisation des entrées
        Plat entree1 = new Plat("Entrée 1", "Description de l'entrée 1", 5.99, "entrée");
        Plat entree2 = new Plat("Entrée 2", "Description de l'entrée 2", 6.99, "entrée");
        Plat entree3 = new Plat("Entrée 3", "Description de l'entrée 3", 4.99, "entrée");
        Plat entree4 = new Plat("Entrée 4", "Description de l'entrée 4", 7.99, "entrée");
        Plat entree5 = new Plat("Entrée 5", "Description de l'entrée 5", 5.49, "entrée");

        ajouterEntree(entree1);
        ajouterEntree(entree2);
        ajouterEntree(entree3);
        ajouterEntree(entree4);
        ajouterEntree(entree5);

        // Initialisation des plats principaux
        Plat platPrincipal1 = new Plat("Plat Principal 1", "Description du plat principal 1", 12.99, "plat principal");
        Plat platPrincipal2 = new Plat("Plat Principal 2", "Description du plat principal 2", 13.99, "plat principal");
        Plat platPrincipal3 = new Plat("Plat Principal 3", "Description du plat principal 3", 11.99, "plat principal");
        Plat platPrincipal4 = new Plat("Plat Principal 4", "Description du plat principal 4", 14.99, "plat principal");
        Plat platPrincipal5 = new Plat("Plat Principal 5", "Description du plat principal 5", 11.49, "plat principal");

        ajouterPlatPrincipal(platPrincipal1);
        ajouterPlatPrincipal(platPrincipal2);
        ajouterPlatPrincipal(platPrincipal3);
        ajouterPlatPrincipal(platPrincipal4);
        ajouterPlatPrincipal(platPrincipal5);

        // Initialisation des desserts
        Plat dessert1 = new Plat("Dessert 1", "Description du dessert 1", 6.99, "dessert");
        Plat dessert2 = new Plat("Dessert 2", "Description du dessert 2", 5.99, "dessert");
        Plat dessert3 = new Plat("Dessert 3", "Description du dessert 3", 7.99, "dessert");
        Plat dessert4 = new Plat("Dessert 4", "Description du dessert 4", 4.99, "dessert");
        Plat dessert5 = new Plat("Dessert 5", "Description du dessert 5", 6.49, "dessert");

        ajouterDessert(dessert1);
        ajouterDessert(dessert2);
        ajouterDessert(dessert3);
        ajouterDessert(dessert4);
        ajouterDessert(dessert5);

        // Initialisation des boissons
        Boisson boisson1 = new Boisson("Boisson 1", "Description de la boisson 1", 2.99);
        Boisson boisson2 = new Boisson("Boisson 2", "Description de la boisson 2", 3.49);
        Boisson boisson3 = new Boisson("Boisson 3", "Description de la boisson 3", 1.99);
        Boisson boisson4 = new Boisson("Boisson 4", "Description de la boisson 4", 2.49);
        Boisson boisson5 = new Boisson("Boisson 5", "Description de la boisson 5", 2.99);

        ajouterBoisson(boisson1);
        ajouterBoisson(boisson2);
        ajouterBoisson(boisson3);
        ajouterBoisson(boisson4);
        ajouterBoisson(boisson5);
    }
    
    public Plat getPlatByNom(String nom) {
        for (Plat plat : entrees) {
            if (plat.getNom().equals(nom)) {
                return plat;
            }
        }
        for (Plat plat : platsPrincipaux) {
            if (plat.getNom().equals(nom)) {
                return plat;
            }
        }
        for (Plat plat : desserts) {
            if (plat.getNom().equals(nom)) {
                return plat;
            }
        }
        return null; // Retourne null si aucun plat correspondant n'est trouvé
    }

    public Boisson getBoissonByNom(String nom) {
        for (Boisson boisson : boissons) {
            if (boisson.getNom().equals(nom)) {
                return boisson;
            }
        }
        return null; // Retourne null si aucune boisson correspondante n'est trouvée
    }
}
