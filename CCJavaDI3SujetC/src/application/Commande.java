package application;

import java.util.ArrayList;
import java.util.List;

public class Commande {
    private List<ItemCommande> items;

    public Commande() {
        this.items = new ArrayList<>();
    }

    public void ajouterItem(ItemCommande item) {
        items.add(item);
    }

    public void supprimerItem(ItemCommande item) {
        items.remove(item);
    }

    public List<ItemCommande> getItems() {
        return items;
    }
}