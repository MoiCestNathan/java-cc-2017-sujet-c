package application;

import java.util.List;

public class MainApplication {

    public static void main(String[] args) {
    	// Création d'une carte de restaurant
        CarteRestaurant carte = new CarteRestaurant();
        carte.initCarte();

        // Création d'une tablee
        Tablee tablee1 = new Tablee(1, 4, "18:30", "20:00");

        // Ajout d'items à la commande de la tablee
        Plat plat1 = carte.getPlatByNom("Plat Principal 1");
        Boisson boisson1 = carte.getBoissonByNom("Boisson 1");
        ItemCommande item1 = new ItemCommande(plat1, boisson1);
        tablee1.getCommande().ajouterItem(item1);

        Plat plat2 = carte.getPlatByNom("Plat Principal 2");
        Boisson boisson2 = carte.getBoissonByNom("Boisson 2");
        ItemCommande item2 = new ItemCommande(plat2, boisson2);
        tablee1.getCommande().ajouterItem(item2);

        // Affichage des items de la commande de la tablee
        List<ItemCommande> items = tablee1.getCommande().getItems();
        for (ItemCommande item : items) {
            System.out.println("Plat : " + item.getPlat().getNom());
            System.out.println("Boisson : " + item.getBoisson().getNom());
            System.out.println("Etat : " + item.getEtat());
            System.out.println("------------------------");
        }

        // Modification de l'état d'un item
        item1.setEtat(EtatItem.PRET_A_ENVOYER);

        // Affichage de l'état modifié
        System.out.println("Nouvel état de l'item 1 : " + item1.getEtat());

        // Suppression d'un plat de la carte
        carte.supprimerPlatPrincipal(plat1);

        // Vérification de la suppression
        List<Plat> platsPrincipaux = carte.getPlatsPrincipaux();
        System.out.println("Nombre de plats principaux après suppression : " + platsPrincipaux.size());
    }
    	
    	
    	
    	/*
    	CarteRestaurant carteRestaurant = new CarteRestaurant();

        carteRestaurant.initCarte();

        System.out.println("Entrées :");
        for (Plat plat : carteRestaurant.getEntrees()) {
            System.out.println(plat.getNom() + " - " + plat.getDescription() + " - Prix : " + plat.getPrix());
        }

        System.out.println("\nPlats principaux :");
        for (Plat plat : carteRestaurant.getPlatsPrincipaux()) {
            System.out.println(plat.getNom() + " - " + plat.getDescription() + " - Prix : " + plat.getPrix());
        }

        System.out.println("\nDesserts :");
        for (Plat plat : carteRestaurant.getDesserts()) {
            System.out.println(plat.getNom() + " - " + plat.getDescription() + " - Prix : " + plat.getPrix());
        }

        System.out.println("\nBoissons :");
        for (Boisson boisson : carteRestaurant.getBoissons()) {
            System.out.println(boisson.getNom() + " - " + boisson.getDescription() + " - Prix : " + boisson.getPrix());
        }*/
}
