package application;

public class Tablee {
    private int numeroTable;
    private int nombreCouverts;
    private String heureArrivee;
    private String heureDepart;
    private Commande commande;

    public Tablee(int numeroTable, int nombreCouverts, String heureArrivee, String heureDepart) {
        this.numeroTable = numeroTable;
        this.nombreCouverts = nombreCouverts;
        this.heureArrivee = heureArrivee;
        this.heureDepart = heureDepart;
        this.commande = new Commande();
    }

    public int getNumeroTable() {
        return numeroTable;
    }

    public int getNombreCouverts() {
        return nombreCouverts;
    }

    public String getHeureArrivee() {
        return heureArrivee;
    }

    public String getHeureDepart() {
        return heureDepart;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }
}