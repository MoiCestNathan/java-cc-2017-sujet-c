package application;

public enum EtatItem {
    EN_ATTENTE,
    A_PREPARER,
    EN_PREPARATION,
    PRET_A_ENVOYER,
    ENVOYE,
    TERMINE
}