package application;

public class ItemCommande {
    private Plat plat;
    private Boisson boisson;
    private EtatItem etat;

    public ItemCommande(Plat plat, Boisson boisson) {
        this.plat = plat;
        this.boisson = boisson;
        this.etat = EtatItem.EN_ATTENTE;
    }

    public Plat getPlat() {
        return plat;
    }

    public Boisson getBoisson() {
        return boisson;
    }

    public EtatItem getEtat() {
        return etat;
    }

    public void setEtat(EtatItem etat) {
        this.etat = etat;
    }
}
